/**
 * PHPの更新の度にブラウザをリロードします。
 */

const gulp = require("gulp");
const config = require("../config");
const setting = config.setting;
const $ = require("gulp-load-plugins")(config.loadPlugins);

gulp.task("php", () => {
	return gulp.src(setting.html.dest + "**/*.php").pipe($.browserSync.reload({ steam: true }));
});
