var $window = $(window);
var $bodyHtml = $('body,html');
var breakPoint1 = 767;

/* ======================================
header sp
====================================== */
$window.scroll(function() {
    if ($(this).scrollTop() > 0) {
        $(".c-header__inner").addClass('is-sroll');
    } else {
        $(".c-header__inner").removeClass('is-sroll');
    }
});

/* ======================================
plus
====================================== */
$(".js-plus").on("click", function(event) {
    event.preventDefault();
    if ($(this).parent().next().is(':animated')) return;
    $(this).toggleClass('is-active');
    $(this).parent().next().slideToggle(500);
});

/* ======================================
top page
====================================== */
$(".c-top p").on("click", function() {
    parent.$bodyHtml.animate({ scrollTop: 0 }, 700);
    return false;
});

/* ======================================
scroll to id
====================================== */
$(".js-anchor a").on("click", function() {
    let toId = $(this).attr('href');
    if ($window.width() > 767) {
        $bodyHtml.animate({ scrollTop: $(toId).offset().top }, 700);
    } else {
        $bodyHtml.animate({ scrollTop: $(toId).offset().top - 60 }, 700);
    }
    return false;
});

/* ======================================
c-slide1
====================================== */
$(".c-slide1").slick({
    dots: true,
    speed: 1000,
    autoplaySpeed: 3500,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    arrows: true,
    nextArrow: '<span class="next"></span>',
    prevArrow: '<span class="pre"></span>',
    focusOnSelect: false,
    pauseOnHover: false,
    accessibility: false
});

/* ======================================
top バナー非表示
====================================== */
setTimeout(() => {
    $('#topSide').contents().find('.c-side__image').hide();
}, 300);
